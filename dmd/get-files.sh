#!/bin/sh
version=$1
wget -O dmd-${version}-src.tar.gz https://github.com/D-Programming-Language/dmd/archive/v${version}.tar.gz
wget -O dmd-druntime-${version}-src.tar.gz https://github.com/D-Programming-Language/druntime/archive/v${version}.tar.gz
wget -O dmd-phobos-${version}-src.tar.gz https://github.com/D-Programming-Language/phobos/archive/v${version}.tar.gz
wget -O dmd-tools-${version}-src.tar.gz https://github.com/D-Programming-Language/tools/archive/v${version}.tar.gz
