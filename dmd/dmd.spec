### ........................................................................................................
# DMD
#
# Some notes for people who build RPMs themselves:
#   You will need libcurl-devel.i686 and libcurl-devel.x86_64 because you may want to build both i686 
#   and x86_64 RPMs. Especially if you are on x86_64 machine. Reason is simple. When you run `dmd -m32` , it
#   will require 32-bit version of libphobos, `dmd -m64` will naturally want 64-bit libphobos.
#
# To build packages for specific platform, execute: rpmbuild -ba --target=<arch> dmd.spec , where <arch> is
#   your target architecture.
#
# Help: https://fedoraproject.org/wiki/How_to_create_an_RPM_package
#
# https://fedoraproject.org/wiki/Packaging:NamingGuidelines#Package_Versioning
# foo-1.1.0-0.1.BETA (this is a prerelease, first beta)
# foo-1.1.0-0.2.BETA1 (this is a prerelease, second beta)
# foo-1.1.0-0.3.BETA2 (this is a prerelease, third beta)
# foo-1.1.0-0.4.CR1 (this is a prerelease, candidate release 1)
# foo-1.1.0-0.5.CR2 (this is a prerelease, candidate release 2)
# foo-1.1.0-1 (final release)
# foo-1.1.0-2.GA1 (post release, GA1)
# foo-1.1.0-3.CP1 (post release, CP1, after GA1)
# foo-1.1.0-4.CP2 (post release, CP2, after CP1)
# foo-1.1.0-5.SP1 (post release, SP1, after CP2)
# foo-1.1.0-6.SP1_CP1 (post release, SP1_CP1, after SP1)
#
# This file is best viewed with 110 columns.
#1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
#

###
# TODO:
#

###
# Definitions
#
%ifarch x86_64
  %global dmd_model 64
%endif
%ifarch i386 i586 i686
  %global dmd_model 32
%endif
%global dmd_major        2
%global dmd_minor        066
%global dmd_release      0
%global dmd_beta         false
%global dmd_beta_version 1
%global dmd_so_release   66
%global phobos_soname    libphobos%{dmd_major}.so.0.%{dmd_so_release}

### ........................................................................................................
# DMD
# 
Name:           dmd
%if %{dmd_beta} == "true"
Version:        %{dmd_major}.%{dmd_minor}.%{dmd_release}-b%{dmd_beta_version}
%else
Version:        %{dmd_major}.%{dmd_minor}.%{dmd_release}
%endif

# This is actually a `build` version:
%if %{dmd_beta} == "true"
Release:        b%{dmd_beta_version}%{?dist}
%else
Release:        4%{?dist}
%endif
    
Summary:        DigitalMars D Compiler
Group:          Development/Languages

# Compiler frontend, standard and runtime libraries, and tools are licensed under GPL.
# Backend is under special license.
License:        GPL
URL:            http://www.dlang.org

Source0: https://github.com/D-Programming-Language/dmd/archive/v%{version}/dmd-%{version}.tar.gz
Source1: https://github.com/D-Programming-Language/druntime/archive/v%{version}/druntime-%{version}.tar.gz
Source2: https://github.com/D-Programming-Language/phobos/archive/v%{version}/phobos-%{version}.tar.gz
Source3: https://github.com/D-Programming-Language/tools/archive/v%{version}/tools-%{version}.tar.gz
# We will get the DMD BASH completion script from the installer.
Source4: https://github.com/D-Programming-Language/installer/archive/v%{version}/installer-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  libcurl-devel

Requires:       libphobos

%description
Compiler for the D Programming language.

### ........................................................................................................
# PHOBOS
# 
# Apparently D packagers agreed to call DMD's Phobos libphobos, GDC Phobos libgphobos and LDC Phobos 
# liblphobos. If, by any chance, people change their mind, we would have to make libphobos a subpackage of
# the 'dmd', so we would end up with dmd-libphobos and dmd-libphobos-devel RPMs.  
#
%package -n libphobos
Requires:       libcurl
Summary:        Standard library for the D programming language.
Group:          System Environment/Libraries

%description -n libphobos
This package contains shared phobos library.

%package -n libphobos-devel
Requires:       libphobos
Group:          Development/Libraries
Summary:        Import files for Phobos.
BuildArch:      noarch

%description -n libphobos-devel
Import files for Phobos.

%package -n libphobos-static
Summary:        Static version of the phobos library.
Group:          System Environment/Libraries

%description -n libphobos-static
This package contains static phobos library.

### ........................................................................................................
# DTOOLS
#
%package tools
Requires:       libcurl-devel
Summary:        Ancilliary tools for the D programming language compiler
Group:          Development/Tools

%description tools
Ancilliary tools for the D programming language compiler


%prep

cd $RPM_BUILD_DIR

# Remove symbolic links if they exist
rm -f dmd druntime
# Remove following directories (if they exist)
rm -Rf dmd-%{version}/ druntime-%{version}/ phobos-%{version}/ tools-%{version}/
 
tar xvzf %{SOURCE1}
tar xvzf %{SOURCE2}
tar xvzf %{SOURCE3}
tar xvzf %{SOURCE4}

# We have to make these symbolic links here because phobos will try to use ../dmd and ../druntime relative
# paths... 
ln -s dmd-%{version} dmd
ln -s druntime-%{version} druntime
ln -s phobos-%{version} phobos

%setup -q

%build

cd $RPM_BUILD_DIR/dmd-%{version}/src
make %{?_smp_mflags} -f posix.mak MODEL=%{dmd_model} RELEASE=1

# We have to use the newly created DMD executable from now on otherwise we may risk compilation to
# fail.
export DMD=$RPM_BUILD_DIR/dmd-%{version}/src/dmd
export DFLAGS="-I$RPM_BUILD_DIR/phobos-%{version} -I$RPM_BUILD_DIR/druntime-%{version}/import -L-L/usr/lib64 -L-L/usr/lib -L-L$RPM_BUILD_DIR/phobos-%{version}/generated/linux/release/%{dmd_model} -L--no-warn-search-mismatch -L--export-dynamic"

cd $RPM_BUILD_DIR/phobos-%{version}
make %{?_smp_mflags} -f posix.mak MODEL=%{dmd_model} RELEASE=1

cd $RPM_BUILD_DIR/tools-%{version}
make %{?_smp_mflags} -f posix.mak PREFIX=%{_bindir} MODEL=%{dmd_model} RELEASE=1

#@configure
#make @{?_smp_mflags}

%install

rm -rf $RPM_BUILD_ROOT

# DMD has no install target. We have to do it manually
#@make_install

cd $RPM_BUILD_DIR/dmd-%{version}
install -Dm755 src/dmd $RPM_BUILD_ROOT%{_bindir}/dmd

install -d $RPM_BUILD_ROOT%{_sysconfdir}
install -Dm644 $RPM_BUILD_DIR/installer-%{version}/linux/dmd-completion $RPM_BUILD_ROOT%{_sysconfdir}/bash_completion.d/dmd

# Let's create the dmd.conf file...
cat << EOF > $RPM_BUILD_ROOT%{_sysconfdir}/dmd.conf
;
; dmd.conf file for dmd
;
; dmd will look for dmd.conf in the following sequence of directories:
;   - current working directory
;   - directory specified by the HOME environment variable
;   - directory dmd resides in
;   - /etc directory
;
; Names enclosed by %% are searched for in the existing environment and inserted
;
; The special name %@P% is replaced with the path to this file
;

[Environment]

; Add -defaultlib=libphobos2.so and -debuglib=libphobos2.so if you want to use Phobos as shared library.

DFLAGS=-I/usr/include/d/dmd -L-L/usr/lib64 -L-L/usr/lib -L--no-warn-search-mismatch -L--export-dynamic -L-lrt

EOF

install -Dm644 docs/man/man1/dmd.1 $RPM_BUILD_ROOT%{_mandir}/man1/dmd.1
install -Dm644 docs/man/man5/dmd.conf.5 $RPM_BUILD_ROOT%{_mandir}/man5/dmd.conf.5

install -Dm644 src/backendlicense.txt $RPM_BUILD_ROOT%{_datadir}/licenses/%{name}/LICENSE-backend
install -Dm644 src/gpl.txt $RPM_BUILD_ROOT%{_datadir}/licenses/%{name}/LICENSE-frontend.1
install -Dm644 src/artistic.txt $RPM_BUILD_ROOT%{_datadir}/licenses/%{name}/LICENSE-frontend.2

install -d $RPM_BUILD_ROOT/%{_datadir}/d/samples
install -Dm644 samples/*.d $RPM_BUILD_ROOT/%{_datadir}/d/samples/

###
# LIBPHOBOS
#
cd $RPM_BUILD_DIR/phobos-%{version}
# Why on the Earth we give this SO name is beyond me...
# posix.mak: SONAME = libphobos2.so.0.$(shell awk -F. '{ print $$2 + 0 }' $(VERSION))
# Why .0. ??
install -Dm755 generated/linux/release/%{dmd_model}/libphobos2.so $RPM_BUILD_ROOT%{_libdir}/%{phobos_soname}

cd $RPM_BUILD_ROOT%{_libdir}
ln -sf %{phobos_soname} libphobos%{dmd_major}.so
ln -sf %{phobos_soname} libphobos.so.%{dmd_major}
ln -sf %{phobos_soname} libphobos.so.%{dmd_major}.%{dmd_minor}

cd $RPM_BUILD_DIR/phobos-%{version}
install -Dm644 LICENSE_1_0.txt $RPM_BUILD_ROOT%{_datadir}/licenses/dmd-phobos/LICENSE

###
# LIBPHOBOS-STATIC
#
# Apparently Fedora guys do not recommend packaging static libraries... I am going to check with them
# and see if we should get rid of this library or not.
install -Dm644 generated/linux/release/%{dmd_model}/libphobos2.a $RPM_BUILD_ROOT%{_libdir}/libphobos2.a

###
# LIBPHOBOS-DEVEL
#

install -d $RPM_BUILD_ROOT/%{_includedir}/d/dmd
cp -r {*.d,etc,std} $RPM_BUILD_ROOT/%{_includedir}/d/dmd/

cp -r $RPM_BUILD_DIR/druntime-%{version}/import/* $RPM_BUILD_ROOT/%{_includedir}/d/dmd/

find $RPM_BUILD_ROOT/usr/include -type f | xargs chmod 0644

install -Dm644 $RPM_BUILD_DIR/druntime-%{version}/LICENSE $RPM_BUILD_ROOT%{_datadir}/licenses/dmd-phobos/LICENSE-druntime

###
# TOOLS
#
# We need to export DFLAGS again for some reason... Probably @install wipes out environment variables, or
# it is a completely new process with different environment... It should be investigated.
export DMD=$RPM_BUILD_DIR/dmd-%{version}/src/dmd
export DFLAGS="-I$RPM_BUILD_DIR/phobos-%{version} -I$RPM_BUILD_DIR/druntime-%{version}/import -L-L/usr/lib64 -L-L/usr/lib -L-L$RPM_BUILD_DIR/phobos-%{version}/generated/linux/release/%{dmd_model} -L--no-warn-search-mismatch -L--export-dynamic"
cd $RPM_BUILD_DIR/tools-%{version}
make -f posix.mak
install -Dm755 generated/linux/%{dmd_model}/dustmite $RPM_BUILD_ROOT%{_bindir}/
make -f posix.mak install
# DMD tools posix.mak file does not have any ideas what DESTDIR is... :( It installs (copies) to ../install
install -Dm755 ../install/bin/*  $RPM_BUILD_ROOT%{_bindir}/
 
# License is the same as libphobos...
install -Dm644 $RPM_BUILD_DIR/phobos-%{version}/LICENSE_1_0.txt $RPM_BUILD_ROOT%{_datadir}/licenses/dmd-tools/LICENSE

%files
%defattr(-,root,root,-)
%{_bindir}/dmd
%{_sysconfdir}/bash_completion.d/dmd

%config
%{_sysconfdir}/dmd.conf

%doc
%{_datadir}/d/samples/*.d
%{_datadir}/licenses/dmd/LICENSE-*
%{_datadir}/man/man1/dmd.1.gz
%{_datadir}/man/man5/dmd.conf.5.gz

%files -n libphobos
%defattr(-,root,root,-)
%{_libdir}/%{phobos_soname}
%{_libdir}/libphobos%{dmd_major}.so
%{_libdir}/libphobos.so.%{dmd_major}
%{_libdir}/libphobos.so.%{dmd_major}.%{dmd_minor}

%doc
%{_datadir}/licenses/dmd-phobos/LICENSE

%files -n libphobos-devel
%defattr(-,root,root,-)
%{_includedir}/d/dmd/*

%doc
%{_datadir}/licenses/dmd-phobos/LICENSE-druntime

%files -n libphobos-static
%defattr(-,root,root,-)
%{_libdir}/libphobos2.a

%files tools
%defattr(-,root,root,-)
#@{_bindir}/dumpobj
#@{_bindir}/obj2asm
%{_bindir}/rdmd
%{_bindir}/ddemangle
%{_bindir}/catdoc
%{_bindir}/detab
%{_bindir}/tolf
%{_bindir}/dget
%{_bindir}/changed
# tools makefile does not install dustmite by default...
%{_bindir}/dustmite

%doc
%{_datadir}/licenses/dmd-tools/LICENSE

%post   -n libphobos 
/sbin/ldconfig

%postun -n libphobos 
/sbin/ldconfig

%changelog
* Sat Aug 23 2014 dejan <dejan@-remove-lekic.org> 2.66.0-4
- Added the missing symbolic link to phobos directory. This was the reason why some dmd tools could not
  build successfully. 

* Thu Mar 6 2014 dejan <dejan@-remove-lekic.org> 2.65.0-4
- Removed -defaultlib from the dmd.conf and added a comment line to it.

* Thu Feb 27 2014 dejan <dejan@-remove-lekic.org> 2.065.0-3
- Added dustmite to the tools RPM.
- Version is now 2.065.0-3

* Sat Jan 11 2014 dejan <dejan@-remove-lekic.org> 2.064.2-12
- Cleanup

* Sat Jan 11 2014 dejan <dejan@-remove-lekic.org> 2.064.2-11
- Used URLs suggested by Martin Nowak. Now we do not have to host source packages on ddn.so. :)

* Tue Nov 19 2013 dejan <dejan@-remove-lekic.org> 2.064.2-10
- This version should compile on machine without installed DMD/libphobos packages. However, looks like
  building fails when there is an older DMD installed. That is a new TODO item... ;)
- By default, DMD will use libphobos2.so . (Experimental)

* Fri Nov 15 2013 Dejan Lekic <dejan@-remove-lekic.org> 2.064.2-4
- Merged /usr/include/d/dmd/druntime and /usr/include/d/dmd/phobos into /usr/include/d/dmd

* Fri Nov 15 2013 dejan <dejan@-remove-lekic.org> 2.064.2-3
- Added BASH completion from the dlang.org RPM file.

* Fri Nov 15 2013 dejan <dejan@-remove-lekic.org> 2.064.2-1
- libphobos-devel is now a noarch RPM. 

* Fri Oct 18 2013 Dejan Lekic <dejan@-remove-lekic.org> 2.062.2-1
- Initial SPEC file.
